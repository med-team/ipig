Source: ipig
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               default-jdk,
               ant,
               libcommons-compress-java,
               libcommons-net-java,
               libjdom1-java,
               libxerces2-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/ipig
Vcs-Git: https://salsa.debian.org/med-team/ipig.git
Homepage: http://sourceforge.net/projects/ipig/
Rules-Requires-Root: no

Package: ipig
Architecture: any
Depends: ${misc:Depends},
         ${java:Depends}
Description: integrating PSMs into genome browser visualisations
 iPiG targets the integration of peptide spectrum matches (PSMs) from
 mass spectrometry (MS) peptide identifications into genomic
 visualisations provided by genome browser such as the UCSC genome
 browser (http://genome.ucsc.edu/).
 .
 iPiG takes PSMs from the MS standard format mzIdentML (*.mzid) or in
 text format and provides results in genome track formats (BED and GFF3
 files), which can be easily imported into genome browsers.
