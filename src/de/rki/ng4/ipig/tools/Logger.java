/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple logging class.
 * 
 * @author Mathias Kuhring
 */
public class Logger {
	
	/**
	 * Initiates a logging file with date and time ("dd.MM.yyyy HH:mm:ss").
	 * 
	 * @param filename name of the log file
	 * @param append true if existing data in the log file should be kept, false else
	 */
	public static void init(String filename, boolean append){
		try {
			BufferedWriter logBuffer = new BufferedWriter(new FileWriter(new File(filename),append));
			logBuffer.write(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date()));
			logBuffer.newLine();
			logBuffer.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Appends text to a logging file.
	 * 
	 * @param filename name of the log file
	 * @param message text to be appended
	 */
	public static void write(String filename, String message){
		try {
			BufferedWriter logBuffer = new BufferedWriter(new FileWriter(new File(filename),true));
			logBuffer.write(message);
			logBuffer.newLine();
			logBuffer.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
