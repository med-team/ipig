/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import de.rki.ng4.ipig.tools.Configurator;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

/**
 * <p>The ProxyDialog is a modal JDialog offering fields to set proxy settings 
 * which are written to the Configurator after submission (Ok button).
 * 
 * @author Mathias Kuhring
 *
 */
@SuppressWarnings("serial")
public class ProxyDialog extends JDialog {

	private JTextField tfHost;
	private JTextField tfPort;
	private JTextField tfUser;
	private JTextField tfPass;
	private JCheckBox chckbxUseAuthenthication;
	
	private JButton btnSet;
	private JButton btnAbort;

	/**
	 * Launch the application.
	 */
	public static void run() {
		try {
			ProxyDialog dialog = new ProxyDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ProxyDialog() {
		// try native look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}

		setModal(true);
		setTitle("Proxy Settings");
		setBounds(100, 100, 350, 203);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);

		JLabel lblHost = new JLabel("Host (ftp):");
		lblHost.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHost.setBounds(10, 11, 50, 14);
		getContentPane().add(lblHost);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPort.setBounds(10, 36, 50, 14);
		getContentPane().add(lblPort);

		JLabel lblUser = new JLabel("User:");
		lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUser.setBounds(10, 61, 50, 14);
		getContentPane().add(lblUser);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setBounds(10, 86, 50, 14);
		getContentPane().add(lblPassword);

		tfHost = new JTextField(Configurator.getProperty("proxyHost", ""));
		tfHost.setBounds(70, 8, 262, 20);
		getContentPane().add(tfHost);
		tfHost.setColumns(10);

		tfPort = new JTextField(Configurator.getProperty("proxyPort", ""));
		tfPort.setColumns(10);
		tfPort.setBounds(70, 33, 262, 20);
		getContentPane().add(tfPort);

		tfUser = new JTextField(Configurator.getProperty("proxyUser", ""));
		tfUser.setEnabled(false);
		tfUser.setColumns(10);
		tfUser.setBounds(70, 58, 262, 20);
		getContentPane().add(tfUser);

		tfPass = new JTextField(Configurator.getProperty("proxyPass", ""));
		tfPass.setEnabled(false);
		tfPass.setBounds(70, 83, 262, 20);
		getContentPane().add(tfPass);

		chckbxUseAuthenthication = new JCheckBox("Use authentication");
		chckbxUseAuthenthication.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxUseAuthenthication.isSelected()) enableAuth(true);
				else enableAuth(false);
			}
		});
		chckbxUseAuthenthication.setBounds(70, 110, 143, 23);
		getContentPane().add(chckbxUseAuthenthication);

		enableAuth(Boolean.parseBoolean(Configurator.getProperty("proxyAuth", "false")));
		
		// pressing the Ok button will write the given proxy settings to the Configurator and close the dialog
		btnSet = new JButton("Ok");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configurator.setProperty("proxyHost", tfHost.getText());
				Configurator.setProperty("proxyPort", tfPort.getText());
				Configurator.setProperty("proxyUser", tfUser.getText());
				Configurator.setProperty("proxyPass", tfPass.getText());
				Configurator.setProperty("proxyAuth", Boolean.toString(chckbxUseAuthenthication.isSelected()));
				dispose();
			}
		});
		btnSet.setBounds(70, 140, 89, 23);
		getContentPane().add(btnSet);

		btnAbort = new JButton("Cancel");
		btnAbort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnAbort.setBounds(169, 140, 89, 23);
		getContentPane().add(btnAbort);
	}
	
	// enables/disables authentication fields
	private void enableAuth(boolean b){
		chckbxUseAuthenthication.setSelected(b);
		tfUser.setEnabled(b);
		tfPass.setEnabled(b);
	}

}
