/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.exceptions;

/**
 * A simple exception used for any kind of format failure in input data.
 * 
 * @author Mathias Kuhring
 */
public class FormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public FormatException(String string) {
		super(string);
	}

}
